<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ url('codemirror/codemirror.css') }}">

    <title>Log Viewer</title>
  </head>
  <body>
    <div class="container-fluid pt-2">
        <div class="d-flex justify-content-between pb-2">
            <div class="flex-grow-1">
                <select id="log-list" class="form-control">
                    @foreach($files as $file)
                    <option value="{{ $file['path'] }}">{{ $file['name'] }}</option>
                    @endforeach
                </select>
            </div>
            <button id="open-file" class="btn btn-primary ml-1">Open</button>
        </div>
        
        <textarea id="file-content" class="form-control"></textarea>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="{{ url('codemirror/codemirror.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var myEditor = CodeMirror.fromTextArea(document.getElementById('file-content'), {
                lineNumbers: true
            });

            myEditor.setSize(null, 800);

            $('#open-file').click(function(){
                var file = $('#log-list option:selected').val();
                $.ajax({
                    // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    url: '{{ route("get-log-content") }}',
                    data: {'log' : file},
                    success: function(res){
                        myEditor.getDoc().setValue(res);
                    },
                    error:function(xmlhttprequest, textstatus, message) {
                        alert('error');
                    }
                });
            });
        });
    </script>
  </body>
</html>
