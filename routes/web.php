<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('cleaner', 'Controller@cleaner');

$router->post('telegram', 'TelegramController@index');
$router->post('invoice-image', 'TelegramController@invoiceImage');
$router->post('new-visitor', 'TelegramController@newVisitor');

$router->post('line', 'LineController@index');
$router->get('push-line', 'LineController@pushLine');

$router->get('log', 'LogController@index');
$router->post('get-log-content', ['as' => 'get-log-content', 'uses' => 'LogController@getLogContent']);

$router->get('bark/shopee-reminder', 'CommonController@shopeeBump');
$router->get('bark/fitness-reminder', 'CommonController@fitness');