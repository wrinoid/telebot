<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Log;

class LogController extends Controller
{
    public function index(Request $request)
    {
        $files = collect(File::files(storage_path('logs')))->sortByDesc(function ($file) {
            return $file->getCTime();
        })
        ->map(function ($file) {
            return [
                'path' => $file->getPathName(),
                'name' => $file->getBaseName()
            ];
        });

        return view('log', [
            'files' => $files
        ]);
    }

    public function getLogContent(Request $request)
    {
        $content = File::get($request->log);

        echo $content;
    }
}
