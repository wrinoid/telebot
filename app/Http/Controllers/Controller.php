<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function cleaner()
    {
        $expired = 24 * 60 * 60;

        if (file_exists(base_path('public'))) {
            foreach (new \DirectoryIterator(base_path('public')) as $fileInfo) {
                if ($fileInfo->isFile() && time() - $fileInfo->getCTime() >= $expired && in_array($fileInfo->getExtension(), ['jpg', 'mp4', 'ogg', 'zip'])) {
                    unlink($fileInfo->getRealPath());
                }
            }
        }
    }
}
