<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Webhook;
use App\Password;
use App\Libraries\Telegram\Telegram;

use Carbon\Carbon;
use Log;

class TelegramController extends Controller
{
    public function index(Request $request)
    {
        $webhook_id = Webhook::insertGetId([
            'source' => 'telegram',
            'response' => json_encode($request->all()),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Log::info('Processing message #'.$webhook_id);

        $telegram = new Telegram($request);

        if ($telegram->type() == 'message') {
            if ($telegram->message->type() == 'text') {
                if (in_array($telegram->message->chatType(), ['group', 'supergroup'])) {
                    // group
                    if ($telegram->message->isMentioned()) {
                        $param = [
                            'chat_id' => $telegram->message->chat['id'],
                            'text' => 'I am only available in private chat.',
                            'reply_to_message_id' => $request['message']['message_id']
                        ];
                        $this->send('sendMessage', $param, $webhook_id, $telegram);
                    }
                } elseif ($telegram->message->chatType() == 'private') {
                    // private
                    if (array_key_exists('username', $telegram->message->from) && $telegram->message->from['username'] == 'wrino8') {
                        $command = $telegram->message->getCommand();

                        switch ($command['command']) {
                            case '/absensi':
                                try {
                                    Log::info('...['.$webhook_id.'] preparing absensi');
                                    $absensi = json_decode(file_get_contents('https://sipeg.azmeela.com/today-attendance'));
                                } catch (\Exception $e) {
                                    Log::info('...['.$webhook_id.'] error, something went wrong');

                                    $message = '⚠️ Tidak dapat terhubung ke server azmeela. Silahkan cek absensi secara manual melalui https://sipeg.azmeela.com';

                                    $param_tele = [
                                        'chat_id' => $telegram->message->chat['id'],
                                        'text' => str_replace('\n', PHP_EOL, $message),
                                        'parse_mode' => 'Markdown'
                                    ];
                                    $this->send('sendMessage', $param_tele, $webhook_id, $telegram);
                                }

                                if ($absensi->is_holiday) {
                                    $message = '📅 '.$absensi->holiday->nama;
                                } else {
                                    $message = '📋 *ABSENSI '.(Carbon::now()->format('d/m/Y')).'*';
                                    foreach ($absensi->absensi as $row) {
                                        if (is_null($row->id_cuti)) {
                                            $emoji = ($row->jam_masuk && $row->jam_keluar) ? '🔵' : '🔴';
                                            $message .= '\n'.$emoji.' '.$row->nama_admin." : ".($row->jam_masuk ? $row->jam_masuk : 'belum tap masuk').", ".($row->jam_keluar ? $row->jam_keluar : 'belum tap keluar');
                                        } else {
                                            $emoji = '🏖';
                                            $message .= '\n'.$emoji.' '.$row->nama_admin." : Cuti";
                                        }
                                    }
                                }

                                Log::info('...['.$webhook_id.'] forwarding to telegram');
                                $param_tele = [
                                    'chat_id' => $telegram->message->chat['id'],
                                    'text' => str_replace('\n', PHP_EOL, $message),
                                    'parse_mode' => 'Markdown'
                                ];
                                $this->send('sendMessage', $param_tele, $webhook_id, $telegram);
                                break;
                            case '/password':
                                $search = $command['attribute'];
                                $result = Password::where(function ($query) use ($search) {
                                    if (!empty($search)) {
                                        $query->where('platform', 'like', '%'.$search.'%');
                                    }

                                    return $query;
                                })->limit(7)->get();

                                $button = [];
                                foreach ($result as $platform) {
                                    $button[] = [
                                        [
                                            "text" => $platform->platform,
                                            "callback_data" => "platform.".$platform->id
                                        ]
                                    ];
                                }

                                if (count($button) == 0) {
                                    $param = [
                                        'chat_id' => $telegram->message->chat['id'],
                                        'text' => 'No data found, you can add a new password using the /setpassword command.'
                                    ];
                                    $this->send('sendMessage', $param, $webhook_id, $telegram);
                                } else {
                                    $param = [
                                        'chat_id' => $telegram->message->chat['id'],
                                        'text' => 'Silahkan pilih...',
                                        'reply_markup' => json_encode([
                                            'inline_keyboard' => $button
                                        ])
                                    ];
                                    $this->send('sendMessage', $param, $webhook_id, $telegram);
                                }
                                break;
                            case '/setpassword':
                                $new_password = $command['attribute'];
                                $data = explode("\n", $new_password);

                                if (count($data) == 4) {
                                    Password::create([
                                        'platform' => $data[0],
                                        'email' => $data[1],
                                        'username' => $data[2],
                                        'password' => encrypt($data[3]),
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now()
                                    ]);

                                    $param = [
                                        'chat_id' => $telegram->message->chat['id'],
                                        'text' => $data[0].' password saved.'
                                    ];
                                    $this->send('sendMessage', $param, $webhook_id, $telegram);

                                    $param = [
                                        'chat_id' => $telegram->message->chat['id'],
                                        'message_id' => $telegram->message->message_id
                                    ];
                                    $this->send('deleteMessage', $param, $webhook_id, $telegram);
                                } else {
                                    $param = [
                                        'chat_id' => $telegram->message->chat['id'],
                                        'text' => 'Please use this format:'.PHP_EOL.'/setpassword'.PHP_EOL.'{platform_name}'.PHP_EOL.'{email}'.PHP_EOL.'{username}'.PHP_EOL.'{password}'
                                    ];
                                    $this->send('sendMessage', $param, $webhook_id, $telegram);
                                }
                                break;
                            case '/editpassword':
                                $new_password = $command['attribute'];
                                $data = explode("\n", $new_password);

                                if (count($data) == 4) {
                                    $exist = Password::find($data[0]);

                                    if ($exist) {
                                        $exist->email = $data[1];
                                        $exist->username = $data[2];
                                        $exist->password = encrypt($data[3]);
                                        $exist->updated_at = Carbon::now();
                                        $exist->save();

                                        $param = [
                                            'chat_id' => $telegram->message->chat['id'],
                                            'text' => $exist->platform.' password updated.'
                                        ];
                                    } else {
                                        $param = [
                                            'chat_id' => $telegram->message->chat['id'],
                                            'text' => 'Cannot found ID '.$data[0]
                                        ];
                                    }

                                    $this->send('sendMessage', $param, $webhook_id, $telegram);

                                    $param = [
                                        'chat_id' => $telegram->message->chat['id'],
                                        'message_id' => $telegram->message->message_id
                                    ];
                                    $this->send('deleteMessage', $param, $webhook_id, $telegram);
                                } else {
                                    $param = [
                                        'chat_id' => $telegram->message->chat['id'],
                                        'text' => 'Please use this format:'.PHP_EOL.'/editpassword'.PHP_EOL.'{platform_id}'.PHP_EOL.'{email}'.PHP_EOL.'{username}'.PHP_EOL.'{password}'
                                    ];
                                    $this->send('sendMessage', $param, $webhook_id, $telegram);
                                }
                                break;
                            default:
                                $param = [
                                    'chat_id' => $telegram->message->chat['id'],
                                    'text' => json_encode($request->all()),
                                    'reply_markup' => json_encode([
                                        'remove_keyboard' => true
                                    ])
                                ];
                                $this->send('sendMessage', $param, $webhook_id, $telegram);
                                break;
                        }
                    }
                }
            } else {
                if (in_array($telegram->message()->chatType(), ['group', 'supergroup'])) {
                    $param = [
                        'chat_id' => $telegram->message->chat['id'],
                        'text' => 'I am only available in private chat.',
                        'reply_to_message_id' => $telegram->message->message_id
                    ];
                    $this->send('sendMessage', $param, $webhook_id, $telegram);
                } elseif ($telegram->message->chatType() == 'private') {
                    $param_tele = [
                        'chat_id' => $telegram->message->chat['id'],
                        'text' => 'I will not respond you. You are not my master.'
                    ];
                    $this->send('sendMessage', $param_tele, $webhook_id, $telegram);
                }
            }
        } elseif ($telegram->type() == 'callback_query') {
            list($key, $value) = array_pad(explode('.', $telegram->callback_query->data), 2, null);

            if ($key == 'platform') {
                $password = Password::where('id', $value)->first();

                $param = [
                    'chat_id' => $telegram->callback_query->message->chat['id'],
                    'message_id' => $telegram->callback_query->message->message_id
                ];
                $this->send('deleteMessage', $param, $webhook_id, $telegram);

                $param = [
                    'chat_id' => $telegram->callback_query->message->chat['id'],
                    'parse_mode' => 'HTML',
                    'text' => 'Here is your password for <b>'.$password->platform.'</b>'.PHP_EOL.'ID : '.$password->id.PHP_EOL.'Email : '.$password->email.PHP_EOL.'Username : '.$password->username.PHP_EOL.'Password : '.decrypt($password->password),
                    'reply_markup' => json_encode([
                        "inline_keyboard" => [
                            [
                                [
                                    "text" => "Copied, please delete.",
                                    "callback_data" => "done"
                                ],
                            ]
                        ]
                    ])
                ];
                $this->send('sendMessage', $param, $webhook_id);
            } elseif ($key == 'done') {
                $param = [
                    'chat_id' => $telegram->callback_query->message->chat['id'],
                    'message_id' => $telegram->callback_query->message->message_id,
                    'text' => 'Deleted.',
                    'parse_mode' => 'HTML'
                ];
                $this->send('editMessageText', $param, $webhook_id);
            }
        }

        return response()->json('OK', 200);
    }

    public function newVisitor(Request $request)
    {
        $param = [
            'chat_id' => -1001317717554,
            'text' => $request['text'],
            'parse_mode' => 'HTML'
        ];

        $this->send('sendMessage', $param);
    }

    public function proofhubTask(Request $request)
    {
        Log::info(json_encode($request->all()));

        $param = [
            'chat_id' => -1001317717554,
            'text' => $request['text'],
            'parse_mode' => 'HTML'
        ];

        $this->send('sendMessage', $param);

        foreach ($request['comments'] as $comment) {
            $param_comment = [
                'chat_id' => -1001317717554,
                'photo' => $comment['url'],
                'caption' => $comment['caption'],
                'parse_mode' => 'HTML'
            ];

            $this->send('sendPhoto', $param_comment);
        }
    }

    public function invoiceImage(Request $request)
    {
        Log::info(json_encode($request->all()));

        $param = [
            'chat_id' => 183282672,
            'photo' => 'https://sipeg.azmeela.com/temp/invoice.jpg?v='.rand(),
            'parse_mode' => 'HTML'
        ];

        $this->send('sendPhoto', $param);
    }

    public function send($type, $param, $webhook_id = null, $telegram = null)
    {
        try {
            Log::info('...'.(!is_null($webhook_id) ? '['.$webhook_id.'] ' : '').'preparing request data');

            if (is_null($telegram)) {
                $telegram = new Telegram();
            }

            $response = $telegram->execute($type, $param);

            if ($webhook_id != null) {
                $webhook_id = trim($webhook_id);
                $webhook_id = str_replace('[', '', $webhook_id);
                $webhook_id = str_replace(']', '', $webhook_id);
            }
            Log::info('...'.(!is_null($webhook_id) ? '['.$webhook_id.'] ' : '').'result : '.$response);
        } catch (\Exception $e) {
            Log::info('...'.(!is_null($webhook_id) ? '['.$webhook_id.'] ' : '').'error : '.$e->getMessage());
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            $response = $e->getResponse();
            Log::info('...'.(!is_null($webhook_id) ? '['.$webhook_id.'] ' : '').'error : '.json_encode($response->getBody()->getContents()));
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            Log::info('...'.(!is_null($webhook_id) ? '['.$webhook_id.'] ' : '').'error : '.json_encode($response->getBody()->getContents()));
        }
    }
}
