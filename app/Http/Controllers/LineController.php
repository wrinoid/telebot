<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Webhook;
use App\LineMember;

use Carbon\Carbon;
use Log;

class LineController extends Controller
{
    public const GROUPID = [
        'C95e34b4801543299280a2e8b130c303b' => [
            'name' => '[Azmeela Crew]'
        ],
        'C6bad08f46f578769fe13e519fcd95b13' => [
            'name' => '[C-Level Azmeela]'
        ],
        'C01aa7f645bca3e366416b0b0800781d2' => [
            'name' => '[Test]'
        ],
    ];

    public function index(Request $request)
    {
        $webhook_id = Webhook::insertGetId([
            'source' => 'line',
            'response' => json_encode($request->all()),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        foreach ($request['events'] as $event) {
            if ($event['type'] == 'message') {
                $this->processMessage($webhook_id, $event);
            } elseif ($event['type'] == 'memberJoined') {
                $this->processMemberJoined($webhook_id, $event);
            } elseif ($event['type'] == 'memberLeft') {
                $this->processMemberLeft($webhook_id, $event);
            }
        }
    }

    private function processMessage($webhook_id, $event)
    {
        Log::info('Processing message #'.$webhook_id);
        Log::info('...['.$webhook_id.'] message type is '.$event['message']['type']);

        if ($event['message']['type'] == 'text') {
            if ($event['source']['type'] == 'group') {
                $profile = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id);
                $name = $profile->displayName;

                $this->insertMember($profile, $event['source']['groupId'], $webhook_id);

                $param_message = [
                    'chat_id' => 183282672,
                    'text' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>'.PHP_EOL.$event['message']['text'],
                    'parse_mode' => 'HTML'
                ];

                if ($event['message']['text'] == '/group_id') {
                    Log::info('...['.$webhook_id.'] message command is /group_id');
                    $param = [
                        'type' => 'text',
                        'text' => $event['source']['groupId']
                    ];
                    $this->sendReplyMessage($event['replyToken'], $param, $webhook_id);

                    Log::info('...['.$webhook_id.'] forwarding to telegram');
                    app(TelegramController::class)->send('sendMessage', $param_message, $webhook_id);
                } elseif ($event['message']['text'] == '/absensi') {
                    Log::info('...['.$webhook_id.'] message command is /absensi');
                    $this->absensi($event['replyToken'], $webhook_id, $param_message);
                } elseif (strpos($event['message']['text'], '/sticker') !== false) {
                    Log::info('...['.$webhook_id.'] message command is /sticker');
                    $sticker = explode(':', $event['message']['text'])[1];
                    $sticker = explode('-', $sticker);
                    $param = [
                        'type' => 'sticker',
                        'packageId' => $sticker[0],
                        'stickerId' => $sticker[1]
                    ];
                    $this->sendReplyMessage($event['replyToken'], $param, $webhook_id);

                    Log::info('...['.$webhook_id.'] forwarding to telegram');
                    app(TelegramController::class)->send('sendMessage', $param_message, $webhook_id);
                } else {
                    Log::info('...['.$webhook_id.'] forwarding to telegram');
                    app(TelegramController::class)->send('sendMessage', $param_message, $webhook_id);
                }
            }
        } elseif ($event['message']['type'] == 'image') {
            if ($event['source']['type'] == 'group') {
                if ($event['message']['contentProvider']['type'] == 'line') {
                    $this->saveImage($event, $webhook_id);

                    $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                    Log::info('...['.$webhook_id.'] forwarding to telegram');
                    // $param = [
                    //     'chat_id' => 183282672,
                    //     'text' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>'.PHP_EOL.'https://bot.wrino.id/image_'.$event['message']['id'].'.jpg',
                    //     'disable_web_page_preview' => false,
                    //     'parse_mode' => 'HTML'
                    // ];
                    // app(TelegramController::class)->send('sendMessage', $param, $webhook_id);

                    $param = [
                        'chat_id' => 183282672,
                        // 'photo' => 'https://img.azmeela.com/q100/https://bot.wrino.id/image_'.$event['message']['id'].'.jpg',
                        'photo' => base_path('public/image_'.$event['message']['id'].'.jpg'),
                        'caption' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>',
                        'parse_mode' => 'HTML'
                    ];
                    app(TelegramController::class)->send('sendPhoto', $param, $webhook_id);

                    Log::info('...['.$webhook_id.'] deleting image');
                    unlink(base_path('public/image_'.$event['message']['id'].'.jpg'));
                } else {
                    $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                    Log::info('...['.$webhook_id.'] forwarding to telegram');
                    $param = [
                        'chat_id' => 183282672,
                        'photo' => $event['message']['contentProvider']['originalContentUrl'],
                        'caption' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>',
                        'parse_mode' => 'HTML'
                    ];

                    app(TelegramController::class)->send('sendPhoto', $param, $webhook_id);
                }
            }
        } elseif ($event['message']['type'] == 'video') {
            if ($event['source']['type'] == 'group') {
                if ($event['message']['contentProvider']['type'] == 'line') {
                    $this->saveVideo($event, $webhook_id);

                    $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                    Log::info('...['.$webhook_id.'] forwarding to telegram');
                    $param = [
                        'chat_id' => 183282672,
                        // 'video' => 'https://img.azmeela.com/q100/https://bot.wrino.id/video_'.$event['message']['id'].'.mp4',
                        'video' => base_path('public/video_'.$event['message']['id'].'.mp4'),
                        'caption' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>',
                        'parse_mode' => 'HTML'
                    ];

                    app(TelegramController::class)->send('sendVideo', $param, $webhook_id);

                    Log::info('...['.$webhook_id.'] deleting video');
                    unlink(base_path('public/video_'.$event['message']['id'].'.mp4'));
                } else {
                    $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                    Log::info('...['.$webhook_id.'] forwarding to telegram');
                    $param = [
                        'chat_id' => 183282672,
                        'video' => $event['message']['contentProvider']['originalContentUrl'],
                        'caption' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>',
                        'parse_mode' => 'HTML'
                    ];

                    app(TelegramController::class)->send('sendVideo', $param, $webhook_id);
                }
            }
        } elseif ($event['message']['type'] == 'audio') {
            if ($event['source']['type'] == 'group') {
                if ($event['message']['contentProvider']['type'] == 'line') {
                    $this->saveVoice($event, $webhook_id);

                    $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                    Log::info('...['.$webhook_id.'] forwarding to telegram');
                    $param = [
                        'chat_id' => 183282672,
                        'voice' => 'https://bot.wrino.id/voice_'.$event['message']['id'].'.ogg',
                        'caption' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>',
                        'parse_mode' => 'HTML'
                    ];

                    app(TelegramController::class)->send('sendVoice', $param, $webhook_id);

                    Log::info('...['.$webhook_id.'] deleting voice');
                    unlink(base_path('public/voice_'.$event['message']['id'].'.ogg'));
                } else {
                    $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                    Log::info('...['.$webhook_id.'] forwarding to telegram');
                    $param = [
                        'chat_id' => 183282672,
                        'voice' => $event['message']['contentProvider']['originalContentUrl'],
                        'caption' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>',
                        'parse_mode' => 'HTML'
                    ];

                    app(TelegramController::class)->send('sendVoice', $param, $webhook_id);
                }
            }
        } elseif ($event['message']['type'] == 'location') {
            if ($event['source']['type'] == 'group') {
                $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                Log::info('...['.$webhook_id.'] forwarding to telegram');
                $param = [
                    'chat_id' => 183282672,
                    'text' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>'.PHP_EOL.'Sent a venue',
                    'parse_mode' => 'HTML'
                ];
                app(TelegramController::class)->send('sendMessage', $param, $webhook_id);

                $param_venue = [
                    'chat_id' => 183282672,
                    'latitude' => $event['message']['latitude'],
                    'longitude' => $event['message']['longitude'],
                    'title' => (array_key_exists('title', $event['message'])) ? $event['message']['title'] : 'No Title',
                    'address' => $event['message']['address']
                ];
                app(TelegramController::class)->send('sendVenue', $param_venue, $webhook_id);
            }
        } elseif ($event['message']['type'] == 'file') {
            if ($event['source']['type'] == 'group') {
                $this->saveFile($event, $webhook_id);

                $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                Log::info('...['.$webhook_id.'] forwarding to telegram');
                $param = [
                    'chat_id' => 183282672,
                    'document' => 'https://bot.wrino.id/file_'.$event['message']['id'].'.zip',
                    'caption' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>'.PHP_EOL.$event['message']['fileName'],
                    'parse_mode' => 'HTML',
                ];
                app(TelegramController::class)->send('sendDocument', $param, $webhook_id);

                Log::info('...['.$webhook_id.'] deleting file');
                unlink(base_path('public/file_'.$event['message']['id'].'-'.$event['message']['fileName']));
                unlink(base_path('public/file_'.$event['message']['id'].'.zip'));
            }
        } elseif ($event['message']['type'] == 'sticker') {
            if ($event['source']['type'] == 'group') {
                $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                Log::info('...['.$webhook_id.'] forwarding to telegram');
                $param = [
                    'chat_id' => 183282672,
                    'text' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>'.PHP_EOL.'/sticker:'.$event['message']['packageId'].'-'.$event['message']['stickerId'],
                    'parse_mode' => 'HTML',
                ];
                app(TelegramController::class)->send('sendMessage', $param, $webhook_id);
            }
        } else {
            if ($event['source']['type'] == 'group') {
                $name = $this->getUser($event['source']['groupId'], $event['source']['userId'], $webhook_id)->displayName;

                Log::info('...['.$webhook_id.'] forwarding to telegram');
                $param = [
                    'chat_id' => 183282672,
                    'text' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.$name.':</b>'.PHP_EOL.$event['message']['type'],
                    'parse_mode' => 'HTML'
                ];

                app(TelegramController::class)->send('sendMessage', $param, $webhook_id);
            }
        }
        Log::info('...['.$webhook_id.'] done processing message');
    }

    private function processMemberJoined($webhook_id, $event)
    {
        if ($event['source']['type'] == 'group') {
            foreach ($event['joined']['members'] as $member) {
                $profile = $this->getUser($event['source']['groupId'], $member['userId'], $webhook_id);

                if (!property_exists($profile, 'message')) {
                    $this->insertMember($profile, $event['source']['groupId'], $webhook_id);
                } else {
                    echo $profile->message;
                }
            }
        }
    }

    private function processMemberLeft($webhook_id, $event)
    {
        if ($event['source']['type'] == 'group') {
            foreach ($event['left']['members'] as $member) {
                $profile = LineMember::where('group_id', $event['source']['groupId'])->where('id', $member['userId'])->first();

                if ($profile) {
                    if ($profile->picture_url) {
                        $param = [
                            'chat_id' => 183282672,
                            'photo' => $profile->picture_url,
                            'caption' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.'</b>'.PHP_EOL.$profile->name.' left the group.',
                            'parse_mode' => 'HTML'
                        ];

                        app(TelegramController::class)->send('sendPhoto', $param, $webhook_id);
                    } else {
                        $param = [
                            'chat_id' => 183282672,
                            'text' => '<b>'.$this::GROUPID[$event['source']['groupId']]['name'].' #'.$webhook_id.PHP_EOL.'</b>'.PHP_EOL.$profile->name.' left the group.',
                            'parse_mode' => 'HTML'
                        ];

                        app(TelegramController::class)->send('sendMessage', $param, $webhook_id);
                    }

                    $profile->delete();
                }
            }
        }
    }

    private function insertMember($profile, $group_id, $webhook_id)
    {
        $check_exist = LineMember::where('id', $profile->userId)->where('group_id', $group_id)->first();

        if (!$check_exist) {
            if (property_exists($profile, 'pictureUrl')) {
                $param = [
                    'chat_id' => 183282672,
                    'photo' => $profile->pictureUrl,
                    'caption' => '<b>'.$this::GROUPID[$group_id]['name'].' #'.$webhook_id.PHP_EOL.'</b>'.PHP_EOL.$profile->displayName.' joined group.',
                    'parse_mode' => 'HTML'
                ];

                app(TelegramController::class)->send('sendPhoto', $param, $webhook_id);
            } else {
                $param = [
                    'chat_id' => 183282672,
                    'text' => '<b>'.$this::GROUPID[$group_id]['name'].' #'.$webhook_id.PHP_EOL.'</b>'.PHP_EOL.$profile->displayName.' joined group.',
                    'parse_mode' => 'HTML'
                ];

                app(TelegramController::class)->send('sendMessage', $param, $webhook_id);
            }
        }

        $member = LineMember::updateOrCreate(
            [
                'id' => $profile->userId,
                'group_id' => $group_id
            ],
            [
                'name' => $profile->displayName,
                'picture_url' => property_exists($profile, 'pictureUrl') ? $profile->pictureUrl : null
            ]
        );
    }

    public function absensi($reply_token, $webhook_id, $param_message)
    {
        try {
            Log::info('...['.$webhook_id.'] preparing absensi');
            $absensi = json_decode(file_get_contents('https://sipeg.azmeela.com/today-attendance'));
        } catch (\Exception $e) {
            Log::info('...['.$webhook_id.'] error, something went wrong');

            $message = '⚠️ Tidak dapat terhubung ke server azmeela. Silahkan cek absensi secara manual melalui https://sipeg.azmeela.com';

            $param = [
                'type' => 'text',
                'text' => $message
            ];
            $this->sendReplyMessage($reply_token, $param, $webhook_id);

            Log::info('...['.$webhook_id.'] forwarding to telegram');
            $param_tele = [
                'chat_id' => 183282672,
                'text' => str_replace('\n', PHP_EOL, $message),
                'parse_mode' => 'Markdown'
            ];
            app(TelegramController::class)->send('sendMessage', $param_tele, $webhook_id);

            return 0;
        }

        if ($absensi->is_holiday) {
            $message = '📅 '.$absensi->holiday->nama;
        } else {
            $message = '📋 *ABSENSI '.(Carbon::now()->format('d/m/Y')).'*';
            foreach ($absensi->absensi as $row) {
                if (is_null($row->id_cuti)) {
                    $emoji = ($row->jam_masuk && $row->jam_keluar) ? '🔵' : '🔴';
                    $message .= '\n'.$emoji.' '.$row->nama_admin." : ".($row->jam_masuk ? $row->jam_masuk : 'belum tap masuk').", ".($row->jam_keluar ? $row->jam_keluar : 'belum tap keluar');
                } else {
                    $emoji = '🏖';
                    $message .= '\n'.$emoji.' '.$row->nama_admin." : Cuti";
                }
            }
        }

        $param = [
            'type' => 'text',
            'text' => $message
        ];
        $this->sendReplyMessage($reply_token, $param, $webhook_id);

        Log::info('...['.$webhook_id.'] forwarding to telegram');
        $param_tele = [
            'chat_id' => 183282672,
            'text' => str_replace('\n', PHP_EOL, $message),
            'parse_mode' => 'Markdown'
        ];
        app(TelegramController::class)->send('sendMessage', $param_tele, $webhook_id);
    }

    public function pushLine()
    {
        try {
            Log::info('...[Automated task] preparing absensi');
            $absensi = json_decode(file_get_contents('https://sipeg.azmeela.com/today-attendance'));
        } catch (\Exception $e) {
            Log::info('...[Automated task] error, something went wrong');
            $message = '⚠️ Tidak dapat terhubung ke server azmeela. Silahkan cek absensi secara manual melalui https://sipeg.azmeela.com';

            $this->sendPushMessage('C95e34b4801543299280a2e8b130c303b', $message);

            Log::info('...[Automated task] forwarding to telegram');
            $param = [
                'chat_id' => 183282672,
                'text' => str_replace('\n', PHP_EOL, $message),
                'parse_mode' => 'Markdown'
            ];
            app(TelegramController::class)->send('sendMessage', $param);

            return 0;
        }

        if ($absensi->is_holiday === false) {
            $message = '📋 *ABSENSI '.(Carbon::now()->format('d/m/Y')).'*';
            foreach ($absensi->absensi as $row) {
                if (is_null($row->id_cuti)) {
                    $emoji = ($row->jam_masuk && $row->jam_keluar) ? '🔵' : '🔴';
                    $message .= '\n'.$emoji.' '.$row->nama_admin." : ".($row->jam_masuk ? $row->jam_masuk : 'belum tap masuk').", ".($row->jam_keluar ? $row->jam_keluar : 'belum tap keluar');
                } else {
                    $emoji = '🏖';
                    $message .= '\n'.$emoji.' '.$row->nama_admin." : Cuti";
                }
            }
        }

        $this->sendPushMessage('C95e34b4801543299280a2e8b130c303b', $message);

        Log::info('...[Automated task] forwarding to telegram');
        $param = [
            'chat_id' => 183282672,
            'text' => str_replace('\n', PHP_EOL, $message),
            'parse_mode' => 'Markdown'
        ];
        app(TelegramController::class)->send('sendMessage', $param);
    }

    public function sendReplyMessage($reply_token, $param, $webhook_id)
    {
        Log::info('...['.$webhook_id.'] sending reply message');
        $message = str_replace('\\\n', '\n', json_encode($param));

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, env('LINE_API_URL').'message/reply');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{"replyToken":"'.$reply_token.'","messages":['.$message.']}');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer '.env('LINE_BEARER_TOKEN');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        Log::info('...['.$webhook_id.'] result : '.$result);
        if (curl_errno($ch)) {
            Log::info('...['.$webhook_id.'] error : '.curl_error($ch));
        }
        curl_close($ch);
    }

    public function sendPushMessage($chat_id, $message)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, env('LINE_API_URL').'message/push');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{"to": "'.$chat_id.'","messages":[{"type":"text","text":"'.$message.'"}]}');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer '.env('LINE_BEARER_TOKEN');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        // Log::info('...result : '.$result);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
            // Log::info('...error : '.curl_error($ch));
        }
        curl_close($ch);
    }

    public function getUser($group_id, $user_id, $webhook_id)
    {
        Log::info('...['.$webhook_id.'] getting user detail');
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, env('LINE_API_URL').'group/'.$group_id.'/member/'.$user_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

        $headers = array();
        $headers[] = 'Authorization: Bearer '.env('LINE_BEARER_TOKEN');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        Log::info('...['.$webhook_id.'] result : '.$result);
        if (curl_errno($ch)) {
            Log::info('...['.$webhook_id.'] error : '.curl_error($ch));
        }
        curl_close($ch);

        return json_decode($result);
    }

    public function saveImage($event, $webhook_id)
    {
        Log::info('...['.$webhook_id.'] saving image');
        $url = env('LINE_API_DATA_URL').'message/'.$event['message']['id'].'/content';
        $options = array('http' => array(
            'method'  => 'GET',
            'header' => 'Authorization: Bearer '.env('LINE_BEARER_TOKEN')
        ));
        $context  = stream_context_create($options);
        $contents = file_get_contents($url, false, $context);
        Log::info('...['.$webhook_id.'] response header: '.json_encode($http_response_header));
        file_put_contents(base_path('public/image_'.$event['message']['id'].'.jpg'), $contents);
        Log::info('...['.$webhook_id.'] mime type: '.mime_content_type(base_path('public/image_'.$event['message']['id'].'.jpg')));
        Log::info('...['.$webhook_id.'] image saved https://bot.wrino.id/image_'.$event['message']['id'].'.jpg');
    }

    public function saveVideo($event, $webhook_id)
    {
        Log::info('...['.$webhook_id.'] saving video');
        $url = env('LINE_API_DATA_URL').'message/'.$event['message']['id'].'/content';
        $options = array('http' => array(
            'method'  => 'GET',
            'header' => 'Authorization: Bearer '.env('LINE_BEARER_TOKEN')
        ));
        $context  = stream_context_create($options);
        file_put_contents(base_path('public/video_'.$event['message']['id'].'.mp4'), file_get_contents($url, false, $context));
        Log::info('...['.$webhook_id.'] video saved https://bot.wrino.id/video_'.$event['message']['id'].'.mp4');
    }

    public function saveVoice($event, $webhook_id)
    {
        Log::info('...['.$webhook_id.'] saving voice');
        $url = env('LINE_API_DATA_URL').'message/'.$event['message']['id'].'/content';
        $options = array('http' => array(
            'method'  => 'GET',
            'header' => 'Authorization: Bearer '.env('LINE_BEARER_TOKEN')
        ));
        $context  = stream_context_create($options);
        file_put_contents(base_path('public/voice_'.$event['message']['id'].'.ogg'), file_get_contents($url, false, $context));
        Log::info('...['.$webhook_id.'] voice saved https://bot.wrino.id/voice_'.$event['message']['id'].'.ogg');
    }

    public function saveFile($event, $webhook_id)
    {
        Log::info('...['.$webhook_id.'] saving file');
        $url = env('LINE_API_DATA_URL').'message/'.$event['message']['id'].'/content';
        $options = array('http' => array(
            'method'  => 'GET',
            'header' => 'Authorization: Bearer '.env('LINE_BEARER_TOKEN')
        ));
        $context  = stream_context_create($options);
        file_put_contents(base_path('public/file_'.$event['message']['id'].'-'.$event['message']['fileName']), file_get_contents($url, false, $context));

        $zip = new \ZipArchive();
        if ($zip->open('file_'.$event['message']['id'].'.zip', \ZipArchive::CREATE) === true) {
            $zip->addFile('file_'.$event['message']['id'].'-'.$event['message']['fileName']);
            $zip->close();
        }
        Log::info('...['.$webhook_id.'] file saved https://bot.wrino.id/file_'.$event['message']['id'].'.zip');
    }
}
