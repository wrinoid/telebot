<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Log;

class CommonController extends Controller
{
    public function shopeeBump(Request $request)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.day.app/easF2S7NS38Xj3nVihknhP/Bump%20Shopee%20Product/Jangan%20lupa%20saatnya%20bump%20shopee%20product?group=shopeeBump&icon=https%3A%2F%2Fwrino.id%2Fwp-content%2Fuploads%2F2022%2F10%2Flogo-1018x1024.png',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    public function fitness(Request $request)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.day.app/easF2S7NS38Xj3nVihknhP/Olahraga!/Ayo%20angkat%20barbelmu%20atau%20push%20up!?group=fitness&icon=https://cdn-icons-png.freepik.com/512/2936/2936886.png',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}
