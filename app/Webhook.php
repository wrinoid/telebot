<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Webhook extends Model
{
    use SoftDeletes;

    protected $table = 't_webhooks';

    public $timestamps = false;

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
