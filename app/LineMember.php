<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LineMember extends Model
{
    use SoftDeletes;

    protected $table = 't_line_member';

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
