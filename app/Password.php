<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Password extends Model
{
    protected $table = 't_passwords';

    public $timestamps = false;

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->connection = 'system';
    }
}
