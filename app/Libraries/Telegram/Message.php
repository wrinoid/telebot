<?php

namespace App\Libraries\Telegram;

class Message
{
    private $attributes;

    public function __construct($attributes)
    {
        $this->attributes = collect($attributes);
    }

    public function type()
    {
        if ($this->attributes->has('text')) {
            return 'text';
        } elseif ($this->attributes->has('photo')) {
            return 'photo';
        }

        return 'undefined';
    }

    public function chatType()
    {
        return $this->attributes['chat']['type'];
    }

    public function isMentioned()
    {
        if (strpos(strtolower($this->attributes['text']), '@wrinobot') !== false) {
            return true;
        }

        return false;
    }

    public function getCommand()
    {
        if ($this->attributes->has('entities')) {
            $command_entity = collect($this->attributes['entities'])->firstWhere('type', 'bot_command');
            if ($command_entity) {
                $command = substr($this->attributes['text'], $command_entity['offset'], $command_entity['length']);
                $attribute = substr($this->attributes['text'], $command_entity['length'], strlen($this->attributes['text']));
            }
        }

        return [
            'command' => isset($command) ? $command : null,
            'attribute' => isset($attribute) ? trim($attribute) : null
        ];
    }

    public function __get($key)
    {
        return $this->attributes[$key];
    }
}
