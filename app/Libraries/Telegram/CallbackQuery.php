<?php

namespace App\Libraries\Telegram;

use App\Libraries\Telegram\Message;

class CallbackQuery
{
    private $attributes;

    public function __construct($attributes)
    {
        $this->attributes = collect($attributes);
        $this->attributes->put('message', $this->setMessage());
    }

    public function setMessage()
    {
        return new Message($this->attributes['message']);
    }

    public function message()
    {
        return $this->attributes['message'];
    }

    public function chatType()
    {
        return $this->attributes['message']['chat']['type'];
    }

    public function __get($key)
    {
        return $this->attributes[$key];
    }
}
