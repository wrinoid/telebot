<?php

namespace App\Libraries\Telegram;

use App\Libraries\Telegram\Message;
use App\Libraries\Telegram\CallbackQuery;

class Telegram
{
    /**
     * The bot username.
     *
     * @var string
     */
    public $botUsername = '@WrinoBot';

    /**
     * The api url.
     *
     * @var string
     */
    private $apiUrl = null;

    /**
     * The attributes payload from webhook.
     *
     * @var \Laravel\Lumen\Routing\Router
     */
    private $attributes;

    public function __construct($attributes = null)
    {
        if (env('TELEGRAM_API_URL') == null) {
            throw new \Exception('The Telegram API URL not defined!');
        }

        $this->apiUrl = env('TELEGRAM_API_URL');
        $this->setAttributes($attributes);
    }

    public function setAttributes($attributes)
    {
        $this->attributes = collect($attributes);
        $this->attributes->put('message', $this->setMessage());
        $this->attributes->put('callback_query', $this->setCallbackQuery());
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function type()
    {
        if ($this->attributes->has('message') && !is_null($this->attributes['message'])) {
            return 'message';
        } elseif ($this->attributes->has('callback_query') && !is_null($this->attributes['callback_query'])) {
            return 'callback_query';
        }

        return 'undefined';
    }

    public function setMessage()
    {
        if ($this->type() == 'message') {
            return new Message($this->attributes['message']);
        }

        return null;
    }

    public function message()
    {
        if ($this->type() == 'message') {
            return $this->attributes['message'];
        }

        return null;
    }

    public function setCallbackQuery()
    {
        if ($this->type() == 'callback_query') {
            return new CallbackQuery($this->attributes['callback_query']);
        }

        return null;
    }

    public function callbackQuery()
    {
        if ($this->type() == 'callback_query') {
            return $this->attributes['callback_query'];
        }

        return null;
    }

    public function execute($type, $param)
    {
        $client = new \GuzzleHttp\Client();

        if ($type == 'sendPhoto') {
            $photo = $param['photo'];
            unset($param['photo']);

            $query = http_build_query($param);
            $request = $client->request('POST', $this->apiUrl.$type.'?'.$query, [
                'multipart' => [
                    [
                        'name'     => 'photo',
                        'contents' => file_get_contents($photo),
                        'filename' => 'image'
                    ]
                ],
            ]);
        } elseif ($type == 'sendVideo') {
            $video = $param['video'];
            unset($param['video']);

            $query = http_build_query($param);
            $request = $client->request('POST', $this->apiUrl.$type.'?'.$query, [
                'multipart' => [
                    [
                        'name'     => 'video',
                        'contents' => file_get_contents($video),
                        'filename' => 'video'
                    ]
                ],
            ]);
        } else {
            $query = http_build_query($param);
            $request = $client->get($this->apiUrl.$type.'?'.$query);
        }

        $response = $request->getBody()->getContents();

        return $response;
    }

    public function __get($key)
    {
        return $this->attributes[$key];
    }
}
